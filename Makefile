SHELL = /bin/sh
INSTALL_DIR = /usr/bin
NAME = iron
VER = 1.0

help:
	@echo "make install     Install ${NAME}."
	@echo "make uninstall   Uninstall ${NAME}."

install:
	cp ${NAME} ${INSTALL_DIR}
	chmod +x ${INSTALL_DIR}/${NAME}

uninstall:
	rm -r ~/.config/${NAME}
	rm ${INSTALL_DIR}/${NAME}

dist:
	mkdir -p ${NAME}-${VER}
	cp ${NAME} ${NAME}.docs.html ${NAME}.docs.md ${NAME}.md LICENSE ${NAME}-${VER}
	tar -cf ${NAME}-${VER}.tar ${NAME}-${VER}
	gzip ${NAME}-${VER}.tar
	rm -rf ${NAME}-${VER}.tar ${NAME}-${VER}
